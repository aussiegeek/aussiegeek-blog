---
title: Mobile Sites
layout: default
---

I am currently working on a mobile site for a customer, initially we decided to go with jQuery mobile, but in the end we've gone with Zepto instead. Main reasons are it's still jQuery like, except it actually works on older slower devices, meaning more happy customers for little pain, and is also much more 'web like'.

The second part was making sure no url changes were required, and if a page didn't have a mobile view, then to revert to the standard view, which ruled out the 'mobile' format hack I've seen in multiple places.

In the end if it's determined the user is wanting a mobile version of the page, I just add the mobile_views directory to the views path, and everything after that just works.

{% highlight ruby %}
def set_mobile_view
  if is_mobile_site?
    prepend_view_path (Rails.root + 'app'+ 'mobile_views').to_s
  end
end
{% endhighlight %}