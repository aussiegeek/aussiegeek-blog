---
title: Empty Views on Android
---

Empty Views on Android
==========


One thing when I was working on TuneLocator for Android, was I couldn't figure out how the empty view for a list view was supposed to work. Turns out in the XML they need to be at the same level

<pre>
&lt;ListView android:id="@android:id/list"
          android:layout_height="fill_parent"
          android:layout_width="fill_parent"/&gt;
&lt;TextView android:id="@id/android:empty"
          android:layout_width="match_parent"
          android:layout_height="match_parent"
          android:gravity="center_vertical|center_horizontal"
          android:text="Finding nearby stations..."/&gt;
</pre>