---
title: Myki Sucks
layout: default
---

As comes as no surprise I quite like my technology and was looking forward to using Myki when I moved to Melbourne, and having used the Go Card in Brisbane was interested to see they'd made improvements such as having periodical tickets able to be sorted on cards (which I see Brisbane will soon have a similar concept (basically you pay no more after your 10th trip for the week))

Unfortunatly it's been nothing like a positive experience all the way along.

The readers really struggle with my Myki in my wallet, so I've hard to take it out to actually use it.

If you setup auto credit card topup, and it fails just once, for any reason, your myki is blocked, and can't be fixed without you posting your Myki back (at your expense), and can take 'up to two weeks'. In the mean time? Too bad, buy another ticket.

The real source of this rant is today's experience. I've been buying 31 day passes for the last couple of months, needlessly touching on and off (I'm not leavint the zones my pass is purchased for, so there is no *real* reason to have to do this), and have been running in to trouble with the old style metcard barriers (or frankenbarriers as I've heard them refferred to). Once again today it wouldn't let me out, so as is easy to do at my 194 cm height, I simply stepped over the barrier where I was stopped by a ticket inspector who wanted to check my ticket (fair enough), and to inform me I had commited an offense (interferring with a barrier), and could be subject to a $180 fine.

After a brief discussion, I was told that I'd be let go with a warning, and that I should get my Myki replaced as it may be defective.

Later that day I went to Southern Cross to get a replacement where I was told, I'd have to go without my Myki for two weeks while a replacement is organised. I was offered a 'courtesy Myki', but until I got the replacement in the mail, I'd have to purchase another pass or use short term tickets, or possibly just buy another 31 day pass as that is the cheapest option.

Hold on? I haven't done anything wrong, and I'm the one that has to fork out more money, and the inconvenience.

I picked a different option, $9.80 admin fee to cancel my Myki, have my current pass refunded + the one that I'd purchased but hadn't yet started + the $20ish credit I had on my Myki, and buy a montly Metcard, and be on my merry way home.

Over the last 12 years of using Metcard, I only remember having a few small issues (mainly leaving my ticket in my jeans pocket and it stopping working electroncially), but as they were tickets that would be rubbish in the most of one month, there simply wasn't this inconvenicne factor.

Fix it:
=======

I'm not just going to rant, I'm going to make some suggestions.

Make sure station staff at any premium station can replace a myki and its passes and any stored value on the spot. It should only take a couple of minutes, and shouldn't need any forms.

In the short term? If I had have been handed a Myki with a two week pass on it, I probably would have put up with it.

Secondly, a failed credit card topup shouldn't disable a card, it just shouldn't have any credit added. Hat tip to the Go Card people when the same problem happen, they rang me, sorted out the problem over the phone and told me it would be fixed overnight (I assume this is because the busses & ferries aren't connected to the network on the go, still annoying but far more reasonable)


Ulterior motive?
================

As we know Myki is in the process of being thurst upon us, and one day Metcard will no longer be an option. Is this a way of dealing with our public transport overcrowing/high demand issues by simply making it painful?